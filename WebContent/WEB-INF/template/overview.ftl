<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>医院介绍</title>
		<link rel="stylesheet" type="text/css" href="css/base.css" />
		<link rel="stylesheet" type="text/css" href="css/home.css" />
		<link rel="stylesheet" type="text/css" href="css/listcontent.css" />
	</head>
	<body>
		<#include "./head.ftl">
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>医院介绍</span></div>
					<div class="list-l-content">
						<div class="content-info">
							<div class="hopspital-pic"><img src="img/bg-ct.jpg" alt="" /></div>
							<p>昌图肛肠医院是一所以治疗肛肠病为主的专科医院，也是昌图县内唯一的肛肠专科医院，是全县肛肠疾病的预防、诊断、治疗中心。是新型农村合作医疗、城镇医疗保险定点报销医院。医院技术力量雄厚，人才密集，专业设备齐全，仪器设备先进，整体医疗水平高，在全县具有良好声誉。医院坚持以中西医结合，微创手术，术中、术后长效止痛为治疗特色。坚持以病人为中心，全心全意为患者服务的理念，在全省率先开展了绿色就医通道。</p>
							<p>医院特聘请沈阳市肛肠医院肛肠外科主任宫学范来我院长期全天出诊，亲自为肛肠病患者进行手术，擅长治疗复杂性痔、肛周脓肿、肛瘘、直肠脱垂、溃疡性结肠炎、结肠息肉、大肠肿瘤及便秘，尤其对各种高位复杂瘘等肛肠疑难病的诊断和治疗具有较深造诣。“微创无痛”手术治疗各类肛肠疾病数万人，具有较高影响力。主任医师王效忠曾任沈阳市肿瘤研究所所长，沈阳市肛肠医院外科主任，沈阳医学院教授、在多种肿瘤、各种肛肠疾病的诊断和治疗方面具有丰富临床经验。普外科主任医师李森源，曾任铁岭市中心医院普外科主任，擅长治疗甲状腺、乳腺、肝胆、无张力疝修补术等，尤其擅长胃癌、结肠癌、直肠癌手术，技术精湛。</p>
							<p>我院还与省级医院建立了协作关系，腔镜中心聘请了中国医科大学四院李卫东教授每周日来我院出诊、开展无痛胃镜、无痛肠镜的检查与治疗，在全县率先开展了胃肠息肉、腺瘤、结肠早癌等无痛氩气刀电切微创手术。昌图肛肠医院拥有多台大型高端精密仪器，如进口奥林巴斯胃镜、奥林巴斯肠镜、飞利浦彩超、氩气刀、全结肠水疗机、高频痔疮治疗机等。昌图肛肠医院随着医院的快速发展加强了与国内著名肛肠诊疗中心联合诊治的脚步，邀请肛肠领域知名的顶级教授、专家现场指导。昌图肛肠医院在强化区域品牌的同时，继续挖掘并优化医院的品牌资源，在政府和社会各界的支持下，为不久的将来把医院打造成辽北区域肛肠病治疗中心和国内知名的肛肠专科医院，为广大患者提供更优质的服务而努力。</p>
						</div>
					</div>
				</div>
				<div class="list-r">
					<div class="r-list">
						<div class="title bg1a"><span>新农合住院实行分段报销</span></div>
						<div class="r-content">
							<dl>
								<dt>0～500元，报销50%；</dt>
								<dt>500～2000元，报销80%；</dt>
								<dt>2000元以上，报销60%；</dt>
								<dt>低保户、五保户在正常报销比例的基础上，再增加15%。</dt>
								<dd>例如：一位患复杂性肛瘘的普通手术患者住院总费用为3000元，报销费用为2050元，个人付费为950元。</dd>
							</dl>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>联系我们</span></div>
						<div class="r-content">
							<ul>
								<li><span>联系电话：</span>74492555</li>
								<li><span>联系地址：</span>昌图火车站东50米</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<#include "./foot.ftl">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/hospital.js"></script>
	</body>
</html>
