<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>科室介绍</title>
		<link rel="stylesheet" type="text/css" href="css/base.css" />
		<link rel="stylesheet" type="text/css" href="css/home.css" />
		<link rel="stylesheet" type="text/css" href="css/listcontent.css" />
		<link rel="stylesheet" type="text/css" href="css/simplePagination.css" />
	</head>
	<body>
		<#include "./head.ftl">

		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>科室介绍</span></div>
					<div class="list-l-content">
						<ul>
							<li>
								<div class="list-title"><a href="javascript:void(0);">疝外科简介</a></div>
								<div class="keshi-list">
									<div class="keshi-pic"><a href="javascript:void(0);"><img width="220" height="120" src="" alt="" /></a></div>
									<div class="keshi-info">昌图肛肠医院疝气科开展疝气无张力修补术取得非常满意的临床效果，尤其对于高龄、慢性气管炎、肺气肿等合并疝的效果都非常良好。传统的疝气修补手术要把缺损周边组织强行缝合起来，术后切口疼痛明显，复发率也比较高。采用无张力疝气修补手术，手术时间短，用网片修补，不增加周围组织张力，术后无明显的疼痛牵扯感。手术当日即可下床活动，住院时间短，术后三天即可出院。临床证明无张力疝修补术是一种最佳的疝气治疗方法。<a class="more" href="javascript:void(0);">[更多]</a></div>
								</div>
							</li>
							<li>
								<div class="list-title"><a href="javascript:void(0);">肛肠外科简介</a></div>
								<div class="keshi-list">
									<div class="keshi-pic"><a href="javascript:void(0);"><img width="220" height="120" src="" alt="" /></a></div>
									<div class="keshi-info">肛肠外科是我院的核心科室，在沈阳市肛肠医院专家宫学范主任的带领下突破多项难题，成功治愈了各种复杂疑难肛肠疾病，特别是采用微创手术，使患者在术中术后基本无痛，全面开展了PPH/TST等肛肠手术，深受本地区和周边地区患者的好评。<a class="more" href="javascript:void(0);">[更多]</a></div>
								</div>
							</li>
							<li>
								<div class="list-title"><a href="javascript:void(0);">疝外科简介</a></div>
								<div class="keshi-list">
									<div class="keshi-pic"><a href="javascript:void(0);"><img width="220" height="120" src="" alt="" /></a></div>
									<div class="keshi-info">昌图肛肠医院疝气科开展疝气无张力修补术取得非常满意的临床效果，尤其对于高龄、慢性气管炎、肺气肿等合并疝的效果都非常良好。传统的疝气修补手术要把缺损周边组织强行缝合起来，术后切口疼痛明显，复发率也比较高。采用无张力疝气修补手术，手术时间短，用网片修补，不增加周围组织张力，术后无明显的疼痛牵扯感。手术当日即可下床活动，住院时间短，术后三天即可出院。临床证明无张力疝修补术是一种最佳的疝气治疗方法。<a class="more" href="javascript:void(0);">[更多]</a></div>
								</div>
							</li>
							<li>
								<div class="list-title"><a href="javascript:void(0);">疝外科简介</a></div>
								<div class="keshi-list">
									<div class="keshi-pic"><a href="javascript:void(0);"><img width="220" height="120" src="" alt="" /></a></div>
									<div class="keshi-info">昌图肛肠医院疝气科开展疝气无张力修补术取得非常满意的临床效果，尤其对于高龄、慢性气管炎、肺气肿等合并疝的效果都非常良好。传统的疝气修补手术要把缺损周边组织强行缝合起来，术后切口疼痛明显，复发率也比较高。采用无张力疝气修补手术，手术时间短，用网片修补，不增加周围组织张力，术后无明显的疼痛牵扯感。手术当日即可下床活动，住院时间短，术后三天即可出院。临床证明无张力疝修补术是一种最佳的疝气治疗方法。<a class="more" href="javascript:void(0);">[更多]</a></div>
								</div>
							</li>
						</ul>
					</div>
					<div class="list-page">
						<div class="page" id="news-page"></div>
					</div>
				</div>
				<div class="list-r">
					<div class="r-list">
						<div class="title bg1a"><span>关于我们</span><a href="javascript:void(0);">更多</a></div>
						<div class="r-content">
							<img src="img/jianjie.jpg" alt="" width="258" height="100" />
							<p>医院是指以向人提供医疗护理服务为主要目的医疗机构。其服务对象不仅包括患者和伤员，也包括处于特定生理状态的健康人（如孕妇、产妇、新生儿）以及完全健康的人。</p>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>新农合住院实行分段报销</span></div>
						<div class="r-content">
							<dl>
								<dt>0～500元，报销50%；</dt>
								<dt>500～2000元，报销80%；</dt>
								<dt>2000元以上，报销60%；</dt>
								<dt>低保户、五保户在正常报销比例的基础上，再增加15%。</dt>
								<dd>例如：一位患复杂性肛瘘的普通手术患者住院总费用为3000元，报销费用为2050元，个人付费为950元。</dd>
							</dl>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>联系我们</span></div>
						<div class="r-content">
							<ul>
								<li><span>联系电话：</span>74492555</li>
								<li><span>联系地址：</span>昌图火车站东50米</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<#include "./foot.ftl">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.simplePagination.js"></script>
		<script type="text/javascript" src="js/hospital.js"></script>
	</body>
</html>
