<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>专家介绍</title>
		<link rel="stylesheet" type="text/css" href="css/base.css" />
		<link rel="stylesheet" type="text/css" href="css/home.css" />
		<link rel="stylesheet" type="text/css" href="css/listcontent.css" />
		<link rel="stylesheet" type="text/css" href="css/simplePagination.css" />
	</head>
	<body>
		<#include "./head.ftl">
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>专家介绍</span></div>
					<div class="list-l-content">
						<ul>
							<li>
								<div class="expert cf">
									<div class="expert-pic"><a href="expertcontent.html"><img src="img/temp/zjpic01.jpg" alt="" width="208" height="223"></a></div>
									<div class="expert-info">
										<div class="info">
											<p><span class="name">王喜阁</span><span class="posi">院长</span></p>
											<p class="shanc">擅长：<span>无痛胃镜、无痛肠镜的检查与治疗</span></p>
											<p class="exp-content">辽宁省中西医结合学会肛肠专业委员会委员，从事肛肠外科、胃肠镜工作近二十年。采用微创手术治疗混合痔、复杂性肛瘘、环状痔PPH术、TST手术。开展痔疮、肛瘘术后的长效止痛新技术。对溃疡性结肠炎、慢性结...</p>
											<a class="exper-btn" href="expertcontent.html">查看详细</a>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="expert cf">
									<div class="expert-pic"><a href="javascript:void(0);"><img src="" alt="" width="208" height="223"></a></div>
									<div class="expert-info">
										<div class="info">
											<p><span class="name">王喜阁</span><span class="posi">院长</span></p>
											<p class="shanc">擅长：<span>无痛胃镜、无痛肠镜的检查与治疗</span></p>
											<p class="exp-content">辽宁省中西医结合学会肛肠专业委员会委员，从事肛肠外科、胃肠镜工作近二十年。采用微创手术治疗混合痔、复杂性肛瘘、环状痔PPH术、TST手术。开展痔疮、肛瘘术后的长效止痛新技术。对溃疡性结肠炎、慢性结...</p>
											<a class="exper-btn" href="javascript:void(0);">查看详细</a>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="expert cf">
									<div class="expert-pic"><a href="javascript:void(0);"><img src="" alt="" width="208" height="223"></a></div>
									<div class="expert-info">
										<div class="info">
											<p><span class="name">王喜阁</span><span class="posi">院长</span></p>
											<p class="shanc">擅长：<span>无痛胃镜、无痛肠镜的检查与治疗</span></p>
											<p class="exp-content">辽宁省中西医结合学会肛肠专业委员会委员，从事肛肠外科、胃肠镜工作近二十年。采用微创手术治疗混合痔、复杂性肛瘘、环状痔PPH术、TST手术。开展痔疮、肛瘘术后的长效止痛新技术。对溃疡性结肠炎、慢性结...</p>
											<a class="exper-btn" href="javascript:void(0);">查看详细</a>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="expert cf">
									<div class="expert-pic"><a href="javascript:void(0);"><img src="" alt="" width="208" height="223"></a></div>
									<div class="expert-info">
										<div class="info">
											<p><span class="name">王喜阁</span><span class="posi">院长</span></p>
											<p class="shanc">擅长：<span>无痛胃镜、无痛肠镜的检查与治疗</span></p>
											<p class="exp-content">辽宁省中西医结合学会肛肠专业委员会委员，从事肛肠外科、胃肠镜工作近二十年。采用微创手术治疗混合痔、复杂性肛瘘、环状痔PPH术、TST手术。开展痔疮、肛瘘术后的长效止痛新技术。对溃疡性结肠炎、慢性结...</p>
											<a class="exper-btn" href="javascript:void(0);">查看详细</a>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="list-page">
						<div class="page" id="news-page"></div>
					</div>
				</div>
				<div class="list-r">
					<div class="r-list">
						<div class="title bg1a"><span>关于我们</span><a href="javascript:void(0);">更多</a></div>
						<div class="r-content">
							<img src="img/jianjie.jpg" alt="" width="258" height="100" />
							<p>医院是指以向人提供医疗护理服务为主要目的医疗机构。其服务对象不仅包括患者和伤员，也包括处于特定生理状态的健康人（如孕妇、产妇、新生儿）以及完全健康的人。</p>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>新农合住院实行分段报销</span></div>
						<div class="r-content">
							<dl>
								<dt>0～500元，报销50%；</dt>
								<dt>500～2000元，报销80%；</dt>
								<dt>2000元以上，报销60%；</dt>
								<dt>低保户、五保户在正常报销比例的基础上，再增加15%。</dt>
								<dd>例如：一位患复杂性肛瘘的普通手术患者住院总费用为3000元，报销费用为2050元，个人付费为950元。</dd>
							</dl>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>联系我们</span></div>
						<div class="r-content">
							<ul>
								<li><span>联系电话：</span>74492555</li>
								<li><span>联系地址：</span>昌图火车站东50米</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<#include "./foot.ftl">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.simplePagination.js"></script>
		<script type="text/javascript" src="js/hospital.js"></script>
	</body>
</html>
