<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>新闻</title>
		<link rel="stylesheet" type="text/css" href="css/base.css" />
		<link rel="stylesheet" type="text/css" href="css/home.css" />
		<link rel="stylesheet" type="text/css" href="css/listcontent.css" />
		<link rel="stylesheet" type="text/css" href="css/simplePagination.css" />
	</head>
	<body>
		<#include "./head.ftl">
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>新闻动态</span></div>
					<div class="list-l-content">
						<ul>
							<li>
								<div class="list-title"><a href="newscontent.html">大肠水疗治疗女性便秘的优势</a></div>
								<p>1、温和无菌：大肠水疗所用水是经过过滤、消毒的34度恒温弱碱性离子水。2、培养有益菌：大肠水疗通过清除肠道宿便、毒素，培养有益菌的生长来改善肠内环境，恢复肠道正肠蠕动功能，增强自身免疫功能。3、器械专用：大肠水疗过程中所用器械均一次性使用，安全、无痛苦、无感染。4、专业舒适：水疗在专门设置的洁净、封闭的舒适环境，由受过培训的女性护士人员操作。<a class="more" href="newscontent.html">[更多]</a></p>
							</li>
							<li>
								<div class="list-title"><a href="javascript:void(0);">女性易便秘的原因</a></div>
								<p>1.由于生理解剖上的差别所致。女性子宫在盆腔内挤压直肠，直肠的弯曲度增大，使大便通过较慢，挤压直肠使大便中的水份被吸收，因而造成硬便难以排出。2.受女性生殖器官的影响，女性肛门前面是阴道，附近的肌肉薄弱，加之月经期充血，妊娠期盆底肌肉松弛，以及分娩时用力过度，也会使阴部的肌肉受损，容易致使便秘发生。<a class="more" href="javascript:void(0);">[更多]</a></p>
							</li>
							<li>
								<div class="list-title"><a href="javascript:void(0);">有哪些人群适合做大肠水疗？</a></div>
								<p> 1.习惯性便秘、结肠炎、痔疮患者。2.预防癌症、痤疮、便秘等疾病的人群。3.皮肤暗淡无光泽、面容憔悴、痤疮、色斑人群。4.头晕、口臭、狐臭、体臭患者。5.需要减肥的人群。6.重视养生及生活 质量的办公室一族。7.预防乳腺增生、卵巢囊肿，子宫肌瘤的成年女性。<a class="more" href="javascript:void(0);">[更多]</a></p>
							</li>
							<li>
								<div class="list-title"><a href="javascript:void(0);">便秘的危害有哪些？</a></div>
								<p>1.影响美容：便秘患者由于粪块长时间滞留肠道，异常发酵，腐败后可产生大量有害的毒素，易生痤疮、面部色素沉着、皮疹等。2.导致肥胖：毒素导致大肠水肿，下半身血液循环减慢，易形成梨形身材及胖肚子。3.产生体臭：毒素的聚集可引起口臭和体臭。<a class="more" href="javascript:void(0);">[更多]</a></p>
							</li>
							<li>
								<div class="list-title"><a href="javascript:void(0);">治疗失眠症会有什么样的方法</a></div>
								<p>治疗失眠症会有什么样的方法 ?我们几乎每个人都有体会过失眠的滋味，但是每个人失眠的原因是不尽相同的，如果我们短时间内的失眠是不会严重的影响到我们的生活的，但是长时间的失眠就会使我们每个人都非常的痛苦，记忆力下降、没有精力做事情等，那么接下来<a class="more" href="javascript:void(0);">[更多]</a></p>
							</li>
						</ul>
					</div>
					<div class="list-page">
						<div class="page" id="news-page"></div>
					</div>
				</div>
				<div class="list-r">
					<div class="r-list">
						<div class="title bg1a"><span>关于我们</span><a href="javascript:void(0);">更多</a></div>
						<div class="r-content">
							<img src="img/jianjie.jpg" alt="" width="258" height="100" />
							<p>医院是指以向人提供医疗护理服务为主要目的医疗机构。其服务对象不仅包括患者和伤员，也包括处于特定生理状态的健康人（如孕妇、产妇、新生儿）以及完全健康的人。</p>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>新农合住院实行分段报销</span></div>
						<div class="r-content">
							<dl>
								<dt>0～500元，报销50%；</dt>
								<dt>500～2000元，报销80%；</dt>
								<dt>2000元以上，报销60%；</dt>
								<dt>低保户、五保户在正常报销比例的基础上，再增加15%。</dt>
								<dd>例如：一位患复杂性肛瘘的普通手术患者住院总费用为3000元，报销费用为2050元，个人付费为950元。</dd>
							</dl>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>联系我们</span></div>
						<div class="r-content">
							<ul>
								<li><span>联系电话：</span>74492555</li>
								<li><span>联系地址：</span>昌图火车站东50米</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<#include "./head.ftl">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.simplePagination.js"></script>
		<script type="text/javascript" src="js/hospital.js"></script>
	</body>
</html>
