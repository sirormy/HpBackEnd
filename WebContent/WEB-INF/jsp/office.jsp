<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>科室介绍</title>
		<jsp:include page="head.jsp" />

		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>科室介绍</span></div>
					<div class="list-l-content">
						<ul>
							<s:iterator value="results['list']" var="o">
							<li>
								<div class="list-title"><a href="javascript:void(0);"><s:property value="#o.name" />简介</a></div>
								<div class="keshi-list">
									<div class="keshi-pic"><a href="javascript:void(0);"><img width="220" height="120" src="<s:property value="#o.image" />" alt="<s:property value="#o.name" />图片" /></a></div>
									<div class="keshi-info"><s:property value="#o.summary" /><a class="more" href="/officeContent?id=<s:property value="#o.id" />">[更多]</a></div>
								</div>
							</li>
							</s:iterator>
						</ul>
					</div>
					<div class="list-page">
						<input type="hidden" id="page" value="<s:property value="results.page" />" />
						<input type="hidden" id="count" value="<s:property value="results.count" />" />
						<input type="hidden" id="limit" value="<s:property value="results.limit" />" />
						<div class="page" id="news-page"></div>
					</div>
				</div>
				<s:action name="sidebar" executeResult="true"></s:action>
			</div>
		</div>
		<jsp:include page="foot.jsp" />
		<script>
		$('.nav li:eq(3) a').addClass('this')
		</script>
	</body>
</html>
