<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>科室介绍</title>
		<jsp:include page="head.jsp" />
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>联系我们</span></div>
					<div class="list-l-content">
						<div class="touch">
							<s:property value="results['L6'].content" escape="false"/>
						</div>
					</div>
				</div>
				<s:action name="sidebar" executeResult="true"></s:action>
			</div>
		</div>
		<!--页脚-->
		<jsp:include page="foot.jsp" />
	</body>
	<script>
	$('.nav li:eq(5) a').addClass('this')
	</script>
</html>

