<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>专家介绍内页</title>
		<jsp:include page="head.jsp" />
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>专家介绍</span>> <span>详细介绍</span></div>
					<div class="list-l-content">
						<div class="expert-pic"><img src="<s:property value="results['expert'].image" />" alt="" width="208" height="223" /></div>
						<div class="expert-info">
							<div class="info">
								<p><span class="name"><s:property value="results['expert'].name" /></span><span class="posi"><s:property value="results['expert'].position" /></span></p>
								<p class="shanc">擅长：<span><s:property value="results['expert'].major" /></span></p>
								<s:property value="results['expert'].content" escape="false" />
							</div>
						</div>
					</div>
				</div>
				<s:action name="sidebar" executeResult="true"></s:action>
			</div>
		</div>
		<jsp:include page="foot.jsp" />
	</body>
</html>
