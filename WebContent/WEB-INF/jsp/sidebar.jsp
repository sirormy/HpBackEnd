<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="list-r">
	<div class="r-list">
		<div class="title bg1a"><span><s:property value="results['L7'].name" /></span><a href="javascript:void(0);">更多</a></div>
		<div class="r-content">
			<s:property value="results['L7'].content" escape="false" />
		</div>
	</div>
	<div class="r-list">
		<div class="title bg1a"><span><s:property value="results['L8'].name" /></span><a href="javascript:void(0);">更多</a></div>
		<div class="r-content">
			<s:property value="results['L8'].content" escape="false" />
		</div>
	</div>
	<div class="r-list">
		<div class="title bg1a"><span><s:property value="results['L9'].name" /></span><a href="javascript:void(0);">更多</a></div>
		<div class="r-content">
			<s:property value="results['L9'].content" escape="false" />
		</div>
	</div>
</div>
