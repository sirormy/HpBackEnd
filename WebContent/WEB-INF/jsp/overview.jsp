<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>医院介绍</title>
		<jsp:include page="head.jsp" />
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>医院介绍</span></div>
					<div class="list-l-content">
						<div class="content-info"><s:property value="results['L5'].content" escape="false"/></div>
					</div>
				</div>
				<s:action name="sidebar" executeResult="true"></s:action>
			</div>
		</div>
		<jsp:include page="foot.jsp" />
	</body>
	<script>
	$('.nav li:eq(1) a').addClass('this')
	</script>
</html>