$(function(){
	 // 利用tab标签与box的顺序对应关系
    $(".tab li a").click(function() {
        $(".tab li a").removeClass("this");
        $(this).addClass("this");
        $(".tab-content").each(function(index){
            if($(".tab li a").eq(index).hasClass("this")) {
                $(this).slideDown();
            }
            else {
                $(this).hide();   
            }
        });
    });
    //侧边栏定位
    var footH = $('.foot').offset().top;
    		listH = $('.list-r').height();
    $(window).scroll(function() {
		if($(this).scrollTop()>570) {
			if($(this).scrollTop()>footH - listH){
				$(".list-r").css({
				"position": "fixed",
				"bottom": "220px",
				"top": "auto",
				"left": "50%",
				"margin-left": "240px",
				"z-index":"1"
				});
			}
			else{
				$(".list-r").css({
				"position": "fixed",
				"top": "0",
				"left": "50%",
				"margin-left": "240px",
				"z-index":"1"
				});
			}
		}
		else {
			
			$(".list-r").css({
				"position": "static",
				"margin-left": "0"
			});
		}
	});
	//分页
	$(function() {
		var $page = $('#news-page');
		if ($('#page').length === 1) {
			var page = $('#page').val()
			var limit = $('#limit').val()
			var count = $('#count').val()
			$page.pagination('destroy')
			if (count > limit) {
				$page.pagination({
					items: count,
					itemsOnPage: limit,
					cssStyle: 'inblock-theme',
					currentPage: page,
					onPageClick: function(page, event) {
						event.preventDefault();
						location.href= location.pathname + '?page='+page
					}
				})
			}
			
		}
	});
	
});










