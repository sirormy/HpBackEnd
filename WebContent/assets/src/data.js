/**
 * Created by Sirormy on 16/1/28.
 */
;(function () {

	// grid数据
	var userCols = [{
		title : "id",
		name : "id"
	}, {
		title : "用户名",
		name : "name"
	}, {
		title : "昵称",
		name : "nickname",
		escape : true
	}]

	window.userCols = userCols

	// 位置列表 列设置
	var locationCols = [{
		title: "id",
		name: "id",
		width: "50"
	}, {
		title: "名称",
		name: "name",
		width: "150"
	}, {
		title: "位置编号",
		name: "code",
		width: 150
	}, {
		title: "操作",
		name: "id",
		renderer: function (val) {
			return '<a class="op-btn" href="javascript:void(0);" data-id="'+val+'">操作</a>'
		}
	}]
	window.locationCols = locationCols

	// 文章列表 列设置
	var articleCols = [{
		title: "id",
		name: "id",
		width: 50,
		hidden: true
	}, {
		title: "标题",
		name: "title",
		width: 300,
		escape: true
	}, {
		title: "添加时间",
		name: "add_time",
		width: 150
	}, {
		title: "简介",
		name: "summary",
		width: 250,
		escape: true
	}, {
		title: "操作",
		name: "id",
		width: 50,
		renderer: function (val) {
			return '<a class="op-btn" href="javascript:void(0);" data-id="'+val+'">操作</a>'
		}
	}]
	window.articleCols = articleCols

	// 科室列表 列设置
	var officeCols = [{
		title: "id",
		name: "id",
		width: 50,
		hidden: true
	}, {
		title: "名称",
		name: "name",
		width: 100,
		escape: true
	}, {
		title: "添加时间",
		name: "add_time",
		width: 150
	}, {
		title: "链接",
		name: "link",
		width: 200,
		renderer: function (val) {
			return '<a href="'+val+'" taget="_blank">'+val+'</a>'
		}
	}, {
		title: "操作",
		name: "id",
		width: 50,
		renderer: function (val) {
			return '<a class="op-btn" href="javascript:void(0);" data-id="'+val+'">操作</a>'
		}
	}]
	window.officeCols = officeCols

	// 专家列表 列设置
	var expertCols = [{
		title: "id",
		name: "id",
		width: 50,
		hidden: true
	}, {
		title: "名称",
		name: "name",
		width: 100,
		escape: true
	}, {
		title: "添加时间",
		name: "add_time",
		width: 150
	}, {
		title: "操作",
		name: "id",
		width: 50,
		renderer: function (val) {
			return '<a class="op-btn" href="javascript:void(0);" data-id="'+val+'">操作</a>'
		}
	}]
	window.expertCols = expertCols
}())


