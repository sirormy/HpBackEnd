/**
 * Created by Sirormy on 16/1/28.
 */
(function ($) {

  /** Tools **/
  // plupload
  var imgUploader = function (settings) {
    var options = {
      browse_button : 'uploadImgBtn',
      runtimes : 'html5,html4,flash',
      multi_selection: false,
      container : 'container',
      url : '/admin/upload',
      flash_swf_url : '/assets/swf/Moxie.swf',
      filters : {
        mime_types : [{
          title : 'Image files',
          extensions : 'jpg,jpeg,png'
        }],
        max_file_size : '2mb',
        prevent_duplicates : false
      },
      init : {
        FilesAdded : function(up, files) {
          plupload.each(files, function(file) {
            up.start();
          });
        },
        FileUploaded : function(up, file, res) {
          var json = JSON.parse(res.response)
          $('.image-src').val(json.url)
        },
        Error: function(up, err) {}
      }
    }
    options = $.extend( {} , options , settings );
    return new plupload.Uploader(options)
  }

  var confirmTip = function (callback) {
    //询问框
    layer.confirm('是否确认删除？', {
      btn: ['确定','取消']
    }, function(){
      callback && callback()
    }, function(){

    })
  }

  var sucTip = function (tip, callback) {
    tip = tip || '操作成功'
    if (typeof tip === 'function') {
      callback = tip
      tip = '操作成功'
    }
    layer.msg(tip, {
    	shade: 0.3,
      icon: 1,
      time: 2000
    }, function(){
      callback && callback()
    })
  }

  var errTip = function (tip, callback) {
    tip = tip || '操作失败'
    if (typeof tip === 'function') {
      callback = tip
      tip = '操作失败'
    }
    layer.msg(tip, {
      shade: 0.3,
      icon: 2,
      time: 2000
    }, function(){
      callback && callback()
    })
  }
  window.sucTip = sucTip
  window.errTip = errTip

  // 富文本
  var editor = function (el) {
    var allowedAttributes = ['width', 'height', 'font-size', 'height', 'line-height', 'color',
      'background', 'margin', 'margin-left', 'margin-top', 'margin-bottom', 'margin-right',
      'padding', 'padding-left', 'padding-top', 'padding-bottom', 'padding-right']
    return new Simditor({
      textarea: el,
      allowedAttributes: allowedAttributes,
      toolbar: ['title', 'bold', 'italic', 'underline', 'strikethrough', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent', 'alignment', '|', 'html'],
      allowedTags: ['br', 'span', 'a', 'img', 'b', 'strong', 'i', 'u', 'p', 'dl', 'dt', 'dd', 'ul', 'ol', 'li', 'blockquote', 'pre', 'code', 'h1', 'h2', 'h3', 'h4', 'hr', 'div'],

    })
  }
  window.editor = editor

  // 这段没有用
  var $opArea = $('#op-area')
  var sourceUrl = location.href
  function jump (url) {}
  $('.menu-item').on('click', function () {
    var $self = $(this)
    var url = $self.data('url')
    jump(url)
    location.hash = url
  })

  /** router **/

  // 用户列表
  $('#itemUserList').on('click', function () {
    $.get('/tpl/userList.html', function (data) {
      $opArea.html(data)
      $('#userList').mmGrid({
        cols : userCols,
        url : "/admin/user/list",
        method: "get",
        root: "items",
        height: "500px",
        nowrap: true,
        plugins : [
          $('#userPager').mmPaginator({
            limitList: [10, 50, 100]
          })
        ]
      })
    })
  })

  // 添加用户
  $('#itemUserAdd').on('click', function () {
    $.get('/tpl/user.html', function (data) {
      $opArea.html(data)
    })
  })

  // 文章列表
  $('#itemArticleList').on('click', function () {
    $.get('/tpl/articleList.html', function (data) {
      $opArea.html(data)
      $('#articleList').mmGrid({
        cols : articleCols,
        url : "/admin/article/list",
        method: "get",
        nowrap: true,
        root: "items",
        height: "500px",
        plugins : [
          $('#userPager').mmPaginator({
            limitList: [10, 50, 100]
          })
        ]
      })
    })
  })

   // 添加文章
  $('#itemArticleAdd').on('click', function () {
    $.get('/tpl/articleAdd.html', function (data) {
      $opArea.html(data)
      // 初始化富文本
      var $editor = editor($('#editor'))
      $('#submitBtn').on('click', function () {
        var title = $('#articleTitle').val()
        var content = $editor.getValue()
        var summary = $('#articleSummary').val()
        $.post('/admin/article/add', {
          "title": title,
          "summary": summary,
          "content": content
        }, function (data) {
          if (data.err) {
            errTip(data.err)
          } else {
            sucTip('操作成功', function () {
              $('#itemArticleList').trigger('click')
            })
          }
        }, 'json')
      })
    })
  })

  // 添加位置
  $('#itemLocationAdd').on('click', function () {
    $.get('/tpl/locationAdd.html', function (data) {
      $opArea.html(data)
      var lvm = new Vue({
        el : '#op-area',
        data : {
          location: data.location,
          content: data.content
        }
      })

      var $editor = editor($('#editor'))
      // 富文本图片上传
      imgUploader().init()
      // 配图图片上传
      imgUploader({
        container: 'imgContainer',
        browse_button: 'imgPicker',
        init : {
          FilesAdded : function(up, files) {
            plupload.each(files, function(file) {
              up.start();
            });
          },
          FileUploaded : function(up, file, res) {
            var json = JSON.parse(res.response)
            $('#contentImg').attr('src', json.url)
            $('#imgUrl').val(json.url)
          }
        }
      }).init()

      $('#submitBtn').on('click', function () {
        var param = {
          link: lvm.$data.location.link,
          code: lvm.$data.location.code,
          image: $('#imgUrl').val(),
          remark: lvm.$data.location.remark,
          content: $editor.getValue()
        }
        $.post('/admin/location/update', param, function (data) {
          if (data.err) {
            alert(data.err)
          } else {
            alert('操作成功')
          }
        }, 'json')
      })
    })
  })

  // 添加专家
  $('#itemExpertAdd').on('click', function () {
    $.get('/tpl/expertAdd.html', function (html) {
      $opArea.html(html)
      var $editor = editor($('#editor'))
      // 富文本图片上传
      imgUploader().init()
      imgUploader({
        container: 'imgContainer',
        browse_button: 'imgPicker',
        init : {
          FilesAdded : function(up, files) {
            plupload.each(files, function(file) {
              up.start();
            });
          },
          FileUploaded : function(up, file, res) {
            var json = JSON.parse(res.response)
            $('#expertImg').attr('src', json.url)
            $('#imgUrl').val(json.url)
          }
        }
      }).init()

      $('#submitBtn').on('click', function () {
        var param = {
          name: $('#expertName').val(),
          summary: $('#summary').val(),
          position: $('#position').val(),
          content: $editor.getValue(),
          major: $('#major').val(),
          image: $('#imgUrl').val()
        }
        $.post('/admin/expert/add', param, function (data) {
          if (data.err) {
            alert(data.err)
          } else {
            alert('操作成功')
          }
        }, 'json')
      })
    })
  })

  // 添加科室
  $('#itemOfficeAdd').on('click', function () {
    $.get('/tpl/officeAdd.html', function (html) {
      $opArea.html(html)
      var $editor = editor($('#editor'))
      // 富文本图片上传
      imgUploader().init()
      // 配图图片上传
      imgUploader({
        container: 'imgContainer',
        browse_button: 'imgPicker',
        init : {
          FilesAdded : function(up, files) {
            plupload.each(files, function(file) {
              up.start();
            });
          },
          FileUploaded : function(up, file, res) {
            var json = JSON.parse(res.response)
            $('#officeImg').attr('src', json.url)
            $('#imgUrl').val(json.url)
          }
        }
      }).init()

      $('#submitBtn').on('click', function () {
        var param = {
          link: $('#contentLink').val(),
          name: $('#officeName').val(),
          image: $('#imgUrl').val(),
          summary: $('#summary').val(),
          content: $editor.getValue()
        }
        $.post('/admin/office/add', param, function (data) {
          if (data.err) {
            alert(data.err)
          } else {
            alert('操作成功')
          }
        }, 'json')
      })
    })
  })

  // 位置列表
  $('#itemLocationList').on('click', function () {
    $.get('/tpl/locationList.html', function (data) {
      $opArea.html(data)
      $('#locationList').mmGrid({
        cols : locationCols,
        url : "/admin/location/list",
        method: "get",
        root: "items",
        height: "500px",
        plugins : [
          $('#locationPager').mmPaginator({
            limitList: [15, 100]
          })
        ]
      })
    })
  })

  // 科室列表
  $('#itemOfficeList').on('click', function () {
    $.get('/tpl/officeList.html', function (data) {
      $opArea.html(data)
      $('#officeList').mmGrid({
        cols : officeCols,
        url : "/admin/office/list",
        method: "get",
        root: "items",
        nowrap: true,
        height: "500px",
        plugins : [
          $('#officePager').mmPaginator({
            limitList: [10, 50, 100]
          })
        ]
      })
    })
  })

  // 专家列表
  $('#itemExpertList').on('click', function () {
    $.get('/tpl/expertList.html', function (data) {
      $opArea.html(data)
      $('#expertList').mmGrid({
        cols : expertCols,
        url : "/admin/expert/list",
        method: "get",
        root: "items",
        nowrap: true,
        height: "500px",
        plugins : [
          $('#expertPager').mmPaginator({
            limitList: [10, 50, 100]
          })
        ]
      })
    })
  })

  /** operational binding **/
  // 修改文章
  $(document).on('click', '#articleList .op-btn', function () {
    var id = $(this).data('id')
    $.get('/tpl/articleUpdate.html', function (html) {
      $opArea.html(html)
      $.get('/admin/article/find?id='+id, function (data) {
        var lvm = new Vue({
          el: '#op-area',
          data: {
            article: data
          }
        })
        var $editor = editor($('#editor'))
        // 富文本图片上传
        imgUploader().init()
        // 配图图片上传
        imgUploader({
          container: 'imgContainer',
          browse_button: 'imgPicker',
          init : {
            FilesAdded : function(up, files) {
              plupload.each(files, function(file) {
                up.start();
              });
            },
            FileUploaded : function(up, file, res) {
              var json = JSON.parse(res.response)
              $('#officeImg').attr('src', json.url)
              $('#imgUrl').val(json.url)
            }
          }
        }).init()

        // 删除
        $('#deleteBtn').on('click', function () {
          confirmTip(function () {
            $.post('/admin/article/delete', {id:id}, function (data) {
              if (data.err) {
                errTip(data.err)
              } else {
                sucTip('操作成功', function () {
                  $('#itemArticleList').trigger('click')
                })
              }
            }, 'json')
          })
        })

        // 提交
        $('#submitBtn').on('click', function () {
          var $article = lvm.$data.article
          var param = {
            id: $article.id,
            title: $article.title,
            name: $article.name,
            summary: $article.summary,
            content: $editor.getValue()
          }
          $.post('/admin/article/update', param, function (data) {
            if (data.err) {
              errTip(data.err)
            } else {
              sucTip('操作成功', function () {
                $('#itemArticleList').trigger('click')
              })
            }
          }, 'json')
        })
      }, 'json')
    })
  })

  // 修改位置内容
  $(document).on('click', '#locationList .op-btn', function () {
    var id = $(this).data('id')
    $.get('/tpl/location.html', function (html) {
      $opArea.html(html)
      $.get('/admin/location/find?id='+id, function (data) {
        var lvm = new Vue({
          el : '#op-area',
          data : {
            location: data.location,
            content: data.content
          }
        })

        var $editor = editor($('#editor'))
        // 富文本图片上传
        imgUploader().init()
        // 配图图片上传
        imgUploader({
          container: 'imgContainer',
          browse_button: 'imgPicker',
          init : {
            FilesAdded : function(up, files) {
              plupload.each(files, function(file) {
                up.start();
              });
            },
            FileUploaded : function(up, file, res) {
              var json = JSON.parse(res.response)
              $('#contentImg').attr('src', json.url)
              $('#imgUrl').val(json.url)
            }
          }
        }).init()

        $('#submitBtn').on('click', function () {
          var param = {
            id: lvm.$data.location.id,
            summary: $('#summary').val(),
            image: $('#imgUrl').val(),
            content: $editor.getValue()
          }
          $.post('/admin/location/update', param, function (data) {
            if (data.err) {
              errTip(data.err)
            } else {
              sucTip('操作成功', function () {
                $('#itemLocationList').trigger('click')
              })
            }
          }, 'json')
        })
      }, 'json')
    })
  })

  // 修改科室
  $(document).on('click', '#officeList .op-btn', function () {
    var id = $(this).data('id')
    $.get('/tpl/officeUpdate.html', function (html) {
      $opArea.html(html)
      $.get('/admin/office/find?id='+id, function (data) {
        var lvm = new Vue({
          el : '#op-area',
          data : {
            office: data.office
          }
        })
        var $editor = editor($('#editor'))
        // 富文本图片上传
        imgUploader().init()
        // 配图图片上传
        imgUploader({
          container: 'imgContainer',
          browse_button: 'imgPicker',
          init : {
            FilesAdded : function(up, files) {
              plupload.each(files, function(file) {
                up.start();
              });
            },
            FileUploaded : function(up, file, res) {
              var json = JSON.parse(res.response)
              $('#officeImg').attr('src', json.url)
              $('#imgUrl').val(json.url)
            }
          }
        }).init()

        // 删除
        $('#deleteBtn').on('click', function () {
          confirmTip(function () {
            $.post('/admin/office/delete', {id:id}, function (data) {
              if (data.err) {
                errTip(data.err)
              } else {
                sucTip('操作成功', function () {
                  $('#itemOfficeList').trigger('click')
                })
              }
            }, 'json')
          })
        })

        $('#submitBtn').on('click', function () {
          var office = lvm.$data.office
          var param = {
            id: office.id,
            link: office.link,
            name: office.name,
            image: $('#imgUrl').val(),
            content : $editor.getValue(),
            summary: office.summary
          }
          $.post('/admin/office/update', param, function (data) {
            if (data.err) {
              errTip(data.err)
            } else {
              sucTip('操作成功', function () {
                $('#itemOfficeList').trigger('click')
              })
            }
          }, 'json')
        })
      }, 'json')
    })
  })

  // 修改专家
  $(document).on('click', '#expertList .op-btn', function () {
    var id = $(this).data('id')
    $.get('/tpl/expertUpdate.html', function (html) {
      $opArea.html(html)
      var $editor = editor($('#editor'))
      // 富文本图片上传
      imgUploader().init()
      imgUploader({
        container: 'imgContainer',
        browse_button: 'imgPicker',
        init : {
          FilesAdded : function(up, files) {
            plupload.each(files, function(file) {
              up.start();
            });
          },
          FileUploaded : function(up, file, res) {
            var json = JSON.parse(res.response)
            $('#expertImg').attr('src', json.url)
            $('#imgUrl').val(json.url)
          }
        }
      }).init()

      $.get('/admin/expert/find?id='+id, function (data) {
        var lvm = new Vue({
          el : '#op-area',
          data : {
            expert: data.expert
          }
        })

        // 删除
        $('#deleteBtn').on('click', function () {
          confirmTip(function () {
            $.post('/admin/expert/delete', {id:id}, function (data) {
              if (data.err) {
                errTip(data.err)
              } else {
                sucTip('操作成功', function () {
                  $('#itemExpertList').trigger('click')
                })
              }
            }, 'json')
          })
        })


        $('#submitBtn').on('click', function () {
          var expert = lvm.$data.expert
          var param = {
            id: expert.id,
            content: $editor.getValue(),
            name: $('#expertName').val(),
            summary: $('#summary').val(),
            position: $('#position').val(),
            major: $('#major').val(),
            image: $('#imgUrl').val()
          }
          $.post('/admin/expert/update', param, function (data) {
            if (data.err) {
              errTip(data.err)
            } else {
              sucTip('操作成功', function () {
                $('#itemExpertList').trigger('click')
              })
            }
          }, 'json')
        })
      }, 'json')
    })
  })

}(jQuery))