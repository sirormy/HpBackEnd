package core.admin.user.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import core.admin.user.dao.UserDao;
import core.comm.BaseDao;
import core.comm.BaseService;
import core.comm.Propertie;
import entity.User;

@Service("userService")
public class UserService extends BaseService<User>{
	
	@Resource
	private UserDao userDao;
	
	public User findByUsername(String username) {
		List<Propertie> props = new ArrayList<Propertie>();
		props.add(new Propertie("where", "name", "=", username));
		return userDao.find(props);
	}
	
	@Resource(name = "userDao")
	public void setBaseDao(BaseDao<User> dao) {
		super.setBaseDao(dao);
	}
}
