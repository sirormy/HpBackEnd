package core.admin.page.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import core.admin.page.dao.PageDao;
import core.comm.BaseDao;
import core.comm.BaseService;
import core.comm.Propertie;
import entity.Page;
import entity.vo.PageVo;

@Service("pageService")
public class PageService extends BaseService<Page>{
	
	@Resource
	private PageDao pageDao;
	
	public List<PageVo> findPageVoList(int pageId) {
		List<Propertie> props = new ArrayList<Propertie>();
		props.add(new Propertie("where", "id", "=", pageId));
		return pageDao.findVoList(props, 0, 0);
	}
	
	@Resource(name = "pageDao")
	public void setBaseDao(BaseDao<Page> dao) {
		super.setBaseDao(dao);
	}
}
