package core.admin.location.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.location.service.ContentService;
import core.admin.location.service.LocationService;
import core.comm.BaseAction;
import core.comm.Propertie;
import entity.Content;
import entity.Location;
import util.BeanUtil;
import util.JSONUtil;


@SuppressWarnings("serial")
@Controller()
@Scope("prototype")
public class LocationAction extends BaseAction{
	private int id;	// 位置id
	private String summary;
	private String content;
	private String image;
	private String name;
	private int page_id;
	private String remark;
	private String code;
	private String link;
	
	private Log logger = LogFactory.getLog(getClass());
	@Resource
	private LocationService locationService;
	@Resource
	private ContentService contentService;
	
	/**
	 * 查询位置列表
	 * @author 王涛
	 * 2016年3月25日
	 */
	public void list () {
		List<Propertie> props = new ArrayList<Propertie>();
		List<Location> locations = locationService.findList(props, page, limit);
		results.put("items", BeanUtil.convertObjectToMapList(locations));
		results.put("page", page);
		results.put("limit", limit);
		results.put("totalCount", locationService.findCount(props));
		JSONUtil.responseJSON(results);
	}
	
	/**
	 * 查询单条的位置及其内容信息
	 * @author 王涛
	 * 2016年3月25日
	 */
	public void find () {
		Location location = locationService.findById(id);
		if (location == null) {
			return;
		}
		Content content = contentService.findByLocationId(id);
		results.put("location", BeanUtil.convertObjectToMap(location));
		if (content != null) {
			results.put("content", BeanUtil.convertObjectToMap(content));
		}
		JSONUtil.responseJSON(results);
	}
	
	/**
	 * 修改位置上的内容信息
	 * @author 王涛
	 * 2016年3月25日
	 */
	public void update () {
		if (id == 0) {
			results.put("err", "参数错误");
			JSONUtil.responseJSON(results);
			return;
		}
		
		Content contentObj = contentService.findByLocationId(id);
		if (contentObj == null) {
			results.put("err", "参数错误");
			JSONUtil.responseJSON(results);
			return;
		}
		
		Location location = locationService.findById(id);
		if (location != null) {
			location.setLink(link);
			try {
				locationService.isUpdate(location);
			} catch (Exception e) {
				logger.error("修改位置失败", e);
				results.put("err", "修改位置失败");
				JSONUtil.responseJSON(results);
				return;
			}
		}
		
		try {
			contentObj.setImage(image);
			contentObj.setSummary(summary);
			contentObj.setContent(content);
			contentService.isUpdate(contentObj);
		} catch (Exception e) {
			logger.error("修改位置失败", e);
			results.put("err", "修改位置失败");
		}
		JSONUtil.responseJSON(results);
	}
	
	/**
	 * 添加位置上的内容信息
	 * @author 王涛
	 * 2016年3月25日
	 */
	public void add () {
		// 保存位置
		Location location = new Location();
		location.setPage_id(page_id);
		location.setName(name);
		location.setLink(link);
		location.setRemark(remark);
		location.setCode(code);
		try {
			locationService.isSave(location);
		} catch (Exception e) {
			logger.error("保存位置失败", e);
			results.put("err", "保存位置失败");
		}
		JSONUtil.responseJSON(results);
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPage_id(int page_id) {
		this.page_id = page_id;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setLink(String link) {
		this.link = link;
	}
}
