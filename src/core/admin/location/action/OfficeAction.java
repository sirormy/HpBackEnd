package core.admin.location.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.location.service.LocationService;
import core.admin.location.service.OfficeService;
import core.comm.BaseAction;
import core.comm.Propertie;
import entity.Office;
import util.JSONUtil;


@Controller()
@Scope("prototype")
@SuppressWarnings("serial")
public class OfficeAction extends BaseAction{
	
	@Resource
	private OfficeService officeService;
	@Resource
	private LocationService locationService;
	
	private Log logger = LogFactory.getLog(getClass());
	private Office office;
	private int id;
	private String name;
	private String summary;
	private String link;
	private String image;
	private String content;
	
	public void list () {
		List<Propertie> props = new ArrayList<Propertie>();
		List<Office> list = officeService.findList(props, page, limit);
		count = officeService.findCount(props);
		List<Map<String, Object>> listMap = new ArrayList<>();
		for (Office o: list) {
			Map<String, Object> m = new HashMap<>();
			m.put("id", o.getId());
			m.put("name", o.getName());
			m.put("link", o.getLink());
			m.put("add_time", o.getAdd_time());
			m.put("image", o.getImage());
			listMap.add(m);
		}
		assembleResults(listMap);
	}
	
	public void add () {
		office = new Office();
		office.setAdd_time(new Date());
		office.setImage(image);
		office.setLink(link);
		office.setName(name);
		office.setContent(content);
		office.setSummary(summary);
		try {
			officeService.isSave(office);
		} catch (Exception e) {
			logger.error("保存失败", e);
			results.put("err", "保存失败");
		}
		JSONUtil.responseJSON(results);
	}
	
	public void find () {
		Office office = officeService.findById(id);
		results.put("office", office);
		JSONUtil.responseJSON(results);
	}
	
	public void update () {
		Office office = officeService.findById(id);
		if (office != null) {
			office.setName(name);
			office.setSummary(summary);
			office.setLink(link);
			office.setContent(content);
			office.setImage(image);
		}
		try {
			officeService.isUpdate(office);
		} catch (Exception e) {
			logger.error("修改失败", e);
			results.put("err", "修改失败");
		}
		JSONUtil.responseJSON(results);
	}
	
	public void delete () {
		try {
			officeService.isDelete(id);
		} catch (Exception e) {
			logger.error("删除科室错误", e);
			results.put("err", "删除失败");
		}
		JSONUtil.responseJSON(results);
	}
	
	
	public Office getOffice() {
		return office;
	}
	public void setOffice(Office office) {
		this.office = office;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
