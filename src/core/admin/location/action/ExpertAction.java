package core.admin.location.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.location.service.ExpertService;
import core.comm.BaseAction;
import core.comm.Propertie;
import entity.Expert;
import util.JSONUtil;


@Controller()
@Scope("prototype")
@SuppressWarnings("serial")
public class ExpertAction extends BaseAction{
	
	private Log logger = LogFactory.getLog(getClass());
	private Expert expert;
	private int id;
	private String name;
	private String summary;
	private String image;
	private String content;
	private String position;	// 职位
	private String major;	// 主治
	
	@Resource
	private ExpertService expertService;
	
	public void list () {
		List<Propertie> props = new ArrayList<Propertie>();
		List<Expert> list = expertService.findList(props, page, 15);
		count = expertService.findCount(props);
		List<Map<String, Object>> listMap = new ArrayList<>();
		for (Expert e: list) {
			Map<String, Object> m = new HashMap<>();
			m.put("id", e.getId());
			m.put("name", e.getName());
			m.put("add_time", e.getAdd_time());
			m.put("image", e.getImage());
			listMap.add(m);
		}
		assembleResults(listMap);
	}
	
	public void add () {
		expert = new Expert();
		expert.setName(name);
		expert.setSummary(summary);
		expert.setImage(image);
		expert.setMajor(major);
		expert.setContent(content);
		expert.setPosition(position);
		expert.setAdd_time(new Date());
		try {
			expertService.isSave(expert);
		} catch (Exception e) {
			logger.error("保存错误", e);
			results.put("err", "保存错误");
		}
		JSONUtil.responseJSON(results);
	}
	
	public void find () {
		Expert expert = expertService.findById(id);
		results.put("expert", expert);
		JSONUtil.responseJSON(results);
	}
	
	public void update () {
		Expert expert = expertService.findById(id);
		if (expert != null) {
			expert.setName(name);
			expert.setSummary(summary);
			expert.setImage(image);
			expert.setContent(content);
			expert.setPosition(position);
			expert.setMajor(major);
			try {
				expertService.isUpdate(expert);
			} catch (Exception e) {
				results.put("err", "修改失败");
			}
		} else {
			results.put("err", "参数错误");
		}
		JSONUtil.responseJSON(results);
	}
	
	/**
	 * 删除专家 
	 */
	public void delete () {
		try {
			expertService.isDelete(id);
		} catch (Exception e) {
			logger.error("删除专家错误", e);
			results.put("err", "删除失败");
		}
		JSONUtil.responseJSON(results);
	}
	
	public void setExpert(Expert expert) {
		this.expert = expert;
	}
	public Expert getExpert() {
		return expert;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public void setPosition(String position) {
		this.position = position;
	}
}
