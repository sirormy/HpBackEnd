package core.admin.location.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import core.admin.location.dao.OfficeDao;
import core.comm.BaseDao;
import core.comm.BaseService;
import entity.Office;

@Service("officeService")
public class OfficeService extends BaseService<Office>{
	@Resource
	private OfficeDao officeDao;
	
	@Resource(name = "officeDao")
	@Override
	public void setBaseDao(BaseDao<Office> baseDao) {
		super.setBaseDao(baseDao);
	}
}
