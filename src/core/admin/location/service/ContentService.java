package core.admin.location.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import core.admin.location.dao.ContentDao;
import core.comm.BaseDao;
import core.comm.BaseService;
import core.comm.Propertie;
import entity.Content;

@Service("contentService")
public class ContentService extends BaseService<Content>{
	
	@Resource
	private ContentDao contentDao;
	
	@Resource(name = "contentDao")
	@Override
	public void setBaseDao(BaseDao<Content> baseDao) {
		super.setBaseDao(baseDao);
	}
	
	
	/**
	 * 根据位置id查找对应内容 
	 * @param locationId 位置id
	 * @author 王涛
	 * 2016年3月25日
	 */
	public Content findByLocationId (int locationId) {
		List<Propertie> props = new ArrayList<Propertie>();
		props.add(new Propertie("where", "location_id", "=", locationId));
		return contentDao.find(props);
	}
}
