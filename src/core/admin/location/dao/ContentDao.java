package core.admin.location.dao;

import org.springframework.stereotype.Repository;

import core.comm.BaseDao;
import entity.Content;

@Repository("contentDao")
public class ContentDao extends BaseDao<Content>{

}
