package core.admin.article.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import Enum.ArticleEnum;
import core.admin.article.service.ArticleService;
import core.comm.BaseAction;
import core.comm.Propertie;
import entity.Article;
import util.AllUtil;
import util.JSONUtil;

@SuppressWarnings("serial")
@Controller("articleAction")
@Scope("prototype")
public class ArticleAction extends BaseAction{
	@Resource
	private ArticleService articleService;
	private Log logger = LogFactory.getLog(getClass());
	private String state;	// 0 未发布 1已发布
	private String title;
	private String content;
	private String summary;
	private int rank;
	private int id;
	
	/**
	 * 文章列表 
	 */
	public void listArticle () {
		List<Propertie> props = new ArrayList<Propertie>();
		if (!AllUtil.isNull(state)) {
			props.add(new Propertie("where", "state", "=", state));
		}
		List<Article> list = articleService.findList(props, page, 10);
		assembleResults(list);
	}
	
	/**
	 * 添加文章 
	 */
	public void addArticle () {
		Article article = new Article(title, content, summary, rank);
		article.setState(ArticleEnum.发布.toString());
		try {
			articleService.isSave(article);
		} catch (Exception e) {
			logger.error("添加文章", e);
			results.put("err", "添加失败");
		}
		JSONUtil.responseJSON(results);
	}
	
	/**
	 * 修改文章 
	 */
	public void updateArticle () {
		List<Propertie> props = new ArrayList<Propertie>();
		props.add(new Propertie("where", "id", "=", id));
		props.add(new Propertie("set", "title", "=", title));
		props.add(new Propertie("set", "content", "=", content));
		articleService.isUpdate(props);
		JSONUtil.responseJSON("");
	}
	
	/**
	 * 查询文章 
	 */
	public void findArticle () {
		Article article = articleService.findById(id);
		JSONUtil.responseJSON(article);
	}
	
	/**
	 * 删除文章 
	 */
	public void deleteArticle () {
		try {
			articleService.isDelete(id);
		} catch (Exception e) {
			logger.error("删除文章错误", e);
			results.put("err", "删除失败");
		}
		JSONUtil.responseJSON(results);
	}
	
	public void setState(String state) {
		this.state = state;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
}
