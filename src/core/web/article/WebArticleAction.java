package core.web.article;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.article.service.ArticleService;
import core.comm.BaseAction;
import entity.Article;


@SuppressWarnings("serial")
@Controller()
@Scope("prototype")
public class WebArticleAction extends BaseAction {
	
	@Resource
	private ArticleService articleService;
	
	/**
	 * 查询文章
	 * 2016年4月5日
	 */
	public String article () {
		Article article = articleService.findById(id);
		if (article != null) {
			results.put("article", article);
			Article prev = articleService.findPrev(id);
			results.put("prev", prev);
			Article next = articleService.findNext(id);
			results.put("next", next);
		}
		return SUCCESS;
	}
	
}
