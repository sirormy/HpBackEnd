package core.comm;


import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.patchca.color.ColorFactory;
import org.patchca.color.SingleColorFactory;
import org.patchca.filter.predefined.CurvesRippleFilterFactory;
import org.patchca.filter.predefined.DiffuseRippleFilterFactory;
import org.patchca.filter.predefined.MarbleRippleFilterFactory;
import org.patchca.filter.predefined.WobbleRippleFilterFactory;
import org.patchca.font.RandomFontFactory;
import org.patchca.service.ConfigurableCaptchaService;
import org.patchca.text.renderer.BestFitTextRenderer;
import org.patchca.text.renderer.TextRenderer;
import org.patchca.utils.encoder.EncoderHelper;
import org.patchca.word.RandomWordFactory;


/**
 * 验证码生成类
 */
public class CaptchaImage extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
	private ConfigurableCaptchaService config = null;	// 配置生成器
	/** 配置参数 */
	private ColorFactory color = null;					// 颜色
	private TextRenderer render = null;					// 效果
	private RandomFontFactory font = null;				// 字体
	private RandomWordFactory words = null;				// 文本
	/** 5种内置干扰样式    */
	private CurvesRippleFilterFactory 	crff = null;  	//干扰线波纹
	private MarbleRippleFilterFactory 	mrff = null;  	//大理石波纹
	private WobbleRippleFilterFactory 	wrff = null;   	//摆波纹
	private DiffuseRippleFilterFactory 	dirff = null;  	//漫波纹

	public CaptchaImage() {
		super();
	}
	
	
	@Override
	public void init() throws ServletException {
		super.init();
		config 	= new ConfigurableCaptchaService();
		
		/** 配置参数 */
		color	= new SingleColorFactory(new Color(25, 60, 170));	// 颜色
		font 	= new RandomFontFactory();
		font.setRandomStyle(false);
		font.setMaxSize(20);
		font.setMinSize(15);
		render 	= new BestFitTextRenderer();
		words 	= new RandomWordFactory();	
		if(null !=this.getInitParameter("wordsCharacters")){
			words.setCharacters(this.getInitParameter("wordsCharacters"));
		}else{
			words.setCharacters("1234567890");
		}
		
		
		
		/** 干扰样式 */
		mrff = new MarbleRippleFilterFactory();
		crff = new CurvesRippleFilterFactory(config.getColorFactory());
		wrff = new WobbleRippleFilterFactory();
		dirff = new DiffuseRippleFilterFactory();
		
		config.setTextRenderer(render);
		config.setFontFactory(font);
		config.setWordFactory(words);
		config.setColorFactory(color);
		config.setWidth(80);
		config.setHeight(26);
	}

	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("image/png");
		response.setHeader("cache", "no-cache");
		if(null !=this.getInitParameter("wordsLength")){
			words.setMaxLength(Integer.parseInt(this.getInitParameter("wordsLength")));
			words.setMinLength(Integer.parseInt(this.getInitParameter("wordsLength")));
		}else{
			words.setMaxLength(4);
			words.setMinLength(4);
		}
		
		HttpSession session = request.getSession(true);
		OutputStream os = response.getOutputStream();
		
		Random r = new Random();
		switch (r.nextInt(3)) {
		case 0:
			//config.setFilterFactory(drff);
			config.setFilterFactory(crff);
			break;
		case 1:
			config.setFilterFactory(wrff);
			//config.setFilterFactory(drff);
			break;
		case 2:
			config.setFilterFactory(wrff);
			break;
		case 3:
			config.setFilterFactory(mrff);
			break;
		case 4:
			config.setFilterFactory(dirff);
			break;
		}
		
		String captcha = EncoderHelper.getChallangeAndWriteImage(config, "png", os);
		session.setAttribute(this.getInitParameter("sessionName"), captcha);
		os.flush();
		os.close();
	}

	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}
	

	@Override
	public void destroy() {
		words = null;
		color  = null;
		font = null;
		config = null;
		super.destroy();
	}

}
