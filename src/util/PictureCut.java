package util;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import config.GlobalConfig;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;


/**
 * 切图共通
 * @author 王国庆
 */
public class PictureCut {
	private static String localUrl = GlobalConfig.SERVER_PATH;
	private static Log logger = LogFactory.getLog(PictureCut.class);
	
	/**
	 * 等宽切图
	 * @param url 原始路径
	 * @param w 宽, 如80
	 * @return
	 */
	public static String CropWImg(String url, String w){
		return (String) sameWidthImg(url, w, Picture.FLAG_YISINET, Picture.VALUE_URL, Picture.IS_NO_FILLER);
	}
	
	/**
	 * 等高切图
	 * @param url 原始路径
	 * @param h 高, 如80
	 * @return
	 */
	public static String CropHImg(String url, String h){
		return (String) sameHeightImg(url, h, Picture.FLAG_YISINET, Picture.VALUE_URL, Picture.IS_NO_FILLER);
	}
	
	/**
	 * 等比缩放图片的接口
	 * @param url
	 * @param w
	 * @param h
	 * @param flag 默认图片类型
	 * @return
	 */
	public static Object CropImg(String url, String w, String h, int flag){
		return geometricImg(url, w+ "x" +h, flag, Picture.VALUE_URL, Picture.IS_NO_FILLER);
	}
	
	/**
	 * 切割  不补白
	 * @param url
	 * @param WxH
	 * @return
	 */
	public static String CropCImg(String url, String WxH){
		return (String) catchImg(url, WxH, Picture.FLAG_YISINET, Picture.VALUE_URL, Picture.IS_FILLER, Picture.TYPE_CUT);
	}
	
	/**
	 * 补白  不切割
	 * @param url
	 * @param WxH
	 * @return
	 */
	public static String CropNCImg(String url, String WxH){
		return (String) catchImg(url, WxH, Picture.FLAG_YISINET, Picture.VALUE_URL, Picture.IS_FILLER, Picture.TYPE_NO_CUT);
	}
	
	/**
	 * 切图的简易接口
	 * @param url
	 * @param w
	 * @param h
	 * @param type 切图方式
	 * @param flag 默认切图类型
	 * @return
	 */
	public static Object CropImg(String url, String w, String h, int type, int flag) {
		if (type == Picture.TYPE_SAME_WIDTH) {
			return sameWidthImg(url, w, flag, Picture.VALUE_URL, Picture.IS_NO_FILLER);
		}else if (type == Picture.TYPE_SAME_HEIGHT) {
			return sameHeightImg(url, h, flag, Picture.VALUE_URL, Picture.IS_NO_FILLER);
		}else{
			return geometricImg(url, w+"x"+h, flag, Picture.VALUE_URL, Picture.IS_NO_FILLER);
		}
	}
	
	/**
	 * 等高切割图片  返回map
	 * @param url
	 * @param h
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> CropImg(String url, String h){
		return (Map<String, Object>) sameHeightImg(url, h, Picture.FLAG_YISINET, Picture.VALUE_MAP, Picture.IS_NO_FILLER);
	}
	
	/**
	 * 等宽切割图片  返回map
	 * @param url
	 * @param h
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> CropWImg(String url, Integer w){
		return (Map<String, Object>) sameWidthImg(url, w.toString(), Picture.FLAG_YISINET, Picture.VALUE_MAP, Picture.IS_NO_FILLER);
	}
	
	/**
	 * 切图总结口
	 * @param url
	 * @param w
	 * @param h
	 * @param type 切图方式
	 * @param flag 默认图片类型
	 * @param value 返回值类型
	 * @param filler 是否补白
	 * @param cut 是否切图
	 * @return
	 */
	public static Object CropImg(String url, String w, String h, int type, int flag, boolean value, boolean filler, boolean cut){
		if (type == Picture.TYPE_SAME_WIDTH) {
			return sameWidthImg(url, w, flag, value, filler);
		}else if (type == Picture.TYPE_SAME_HEIGHT) {
			return sameHeightImg(url, h, flag, value, filler);
		}else if (type == Picture.TYPE_GEOMETRIC) {
			return geometricImg(url, w+"x"+h, flag, value, filler);
		}else{
			return catchImg(url, w+"x"+h, flag, value, filler, cut);
		}
	}
	
	private static String DefaultImg(int flag){
		if (flag == Picture.FLAG_CRY) {
			return "/upload/default/cry.jpg";
		}else if (flag == Picture.FLAG_LOGO) {
			return "/upload/default/logo.jpg";
		}else if (flag == Picture.FLAG_OG){
			return "/upload/default/og.jpg";
		}else {
			return "/upload/default/yisinet.jpg";
		}
	}
	
	/**
	 * 用途等宽缩放图片
	 * 默认高质量缩放切割方式
	 * @author 	王国庆
	 * @param 	url  	上传文件的目录地址
	 * @param 	W		宽度
	 * @return	切后图片的url地址
	 * @throws 	Exception
	 */
	private static Object sameWidthImg(String url, String W, int flag, boolean value, boolean filler){
		String _temp = "";
		try {
			//判断url是否为空
			if (AllUtil.isNull(url) || AllUtil.isNull(W)) {
				_temp = DefaultImg(flag);
			}
			
			url = url.replaceAll("\\\\", "/");
			//获取文件名和文件类型
			String fileName = url.substring(url.lastIndexOf("/")+1, url.lastIndexOf("."));
			String ext = url.substring(url.lastIndexOf("."));
			
			String local = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/width/"+W+"/";	//切图的文件夹地址
			String new_url = local+fileName+ext;	//拼写切图的文件地址
			if (new_url.endsWith(".bmp")) {
				new_url = new_url.replace(".bmp", ".jpg");
			}
			
			File filesCheck = new File(localUrl + url);			//原图的文件流
			if (!filesCheck.exists() || filesCheck.length() == 0) {
				url = DefaultImg(flag);
				local = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/width/"+W+"/"+flag+"/";
				new_url = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/width/"+W+"/"+flag+"/default"+ext;
				if (new_url.endsWith(".bmp")) {
					new_url = new_url.replace(".bmp", ".jpg");
				}
				
				filesCheck = new File(localUrl + url);
			}
			
			File files = new File(new_url);						//切图的文件流
			
			/* 判断图片存在时是否切割 */
			if (files.exists() &&files.length() != 0) {
				_temp = "/"+new_url.replace(localUrl, "").replace("//", "/");
			} else if(ext.equals(".gif")){
				_temp = url;
			} else if (!filesCheck.exists() || filesCheck.length() == 0) {
				_temp = DefaultImg(flag);
			}else {
				BufferedImage sourceImg = ImageIO.read(filesCheck);
				
				// 图片的原始宽高
				int height = sourceImg.getHeight();
				int width = sourceImg.getWidth();
				
				// 图片的目标宽
				int w = Integer.parseInt(W);
				
				int targetWidth = 0;
				int targetHeight = 0;
				
				File file = new File(local);
				if (!file.exists()) {
					file.mkdirs();
				}
				
				String tail = ext.substring(1);
				if (".bmp".equals(ext)) {
					tail = "jpg";
				}
				
				if (width < w) {
					targetWidth = width;
					targetHeight = height;
					
					Thumbnails.of(localUrl + url).forceSize(targetWidth, targetHeight)
						.outputFormat(tail).outputQuality(1.0f).toFile(new_url);
					
					//切图需要补白
					if (filler) {
						String urls = negative(localUrl + url, w, targetHeight);
						new_url = mergePic(urls, new_url, w, targetHeight);
						
						File fileNeg = new File(urls);
						if (fileNeg.exists()) {
							fileNeg.delete();
						}
					}
					
					_temp = "/" + new_url.replace(localUrl, "").replace("//", "/");
				}else {
					//图片的缩放后的宽高
					targetWidth = w;
					targetHeight = (int)((float)w/width*height);
					
					//生成目标图片大小
					Thumbnails.of(localUrl + url).forceSize(targetWidth, targetHeight)
						.outputFormat(tail).outputQuality(1.0f).toFile(new_url);
					
					_temp = "/"+new_url.replace(localUrl, "").replace("//", "/");
				}
			}
		} catch (Exception e) {
			logger.info("切割图片失败"+url);
			_temp = DefaultImg(flag);
		}
		
		//判断需要的返回值类型
		if (value) {
			Map<String, Object> _map =  new HashMap<String, Object>();
			FileInputStream is;
			try {
				is = new FileInputStream(localUrl + _temp);
				BufferedImage sourceImg = javax.imageio.ImageIO.read(is);
				
				_map.put("url", _temp);
				_map.put("width", sourceImg.getWidth());
				_map.put("height", sourceImg.getHeight());
			} catch (Exception e) {
				logger.info("读取图片失败"+url);
			}
			
			return _map;
		}else {
			return _temp;
		}
	}
	
	/**
	 * 用途等高缩放图片
	 * @author 王国庆
	 * @param url  上传文件的目录地址
	 * @param local  缓存文件的除参数外的存放地址
	 * @param Q  缓存文件的图片地址0、低 1、中 2、高
	 * @param T 0、缩放不切割 1、缩放切割
	 * @param WxH 目的图片的大小
	 * @throws Exception
	 */
	private static Object sameHeightImg(String url, String H, int flag, boolean value, boolean filler){
		String _temp = "";
		try {
			if (AllUtil.isNull(url) || AllUtil.isNull(H)) {
				url = DefaultImg(flag);
			}
			
			url = url.replaceAll("\\\\", "/");
			//获取文件名和文件类型
			String fileName = url.substring(url.lastIndexOf("/")+1, url.lastIndexOf("."));
			String ext = url.substring(url.lastIndexOf("."));
			
			String local = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/height/"+H+"/";	//切图的文件夹地址
			String new_url = local+fileName+ext;	//拼写切图的文件地址
			if (new_url.endsWith(".bmp")) {
				new_url = new_url.replace(".bmp", ".jpg");
			}
			
			File filesCheck = new File(localUrl + url);			//原图的文件流
			if (!filesCheck.exists() || filesCheck.length() == 0) {
				url = DefaultImg(flag);
				local = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/height/"+H+"/"+flag+"/";
				new_url = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/height/"+H+"/"+flag+"/default"+ext;
				if (new_url.endsWith(".bmp")) {
					new_url = new_url.replace(".bmp", ".jpg");
				}
				
				filesCheck = new File(localUrl + url);
			}
			
			File files = new File(new_url);						//切图的文件流
			
			/* 判断图片存在时是否切割 */
			if (files.exists() &&files.length() != 0) {
				_temp = "/"+new_url.replace(localUrl, "").replace("//", "/");
			} else if(ext.equals(".gif")){
				_temp = url;
			} else {
				BufferedImage sourceImg = ImageIO.read(filesCheck);
				
				//图片的原始宽高
				int height = sourceImg.getHeight();
				int width = sourceImg.getWidth();
				//图片的目标宽高
				int h = Integer.parseInt(H);
				
				int targetWidth = 0;
				int targetHeight = 0;
				
				File file = new File(local);
				if (!file.exists()) {
					file.mkdirs();
				}
				
				String tail = ext.substring(1);
				if (".bmp".equals(ext)) {
					tail = "jpg";
				}
				if (height < h) {
					targetWidth = width;
					targetHeight = height;
					
					Thumbnails.of(localUrl + url).forceSize(targetWidth, targetHeight)
						.outputFormat(tail).outputQuality(1.0f).toFile(new_url);
					
					//切图需要补白
					if (filler) {
						String urls = negative(localUrl + url, targetWidth, h);
						new_url = mergePic(urls, new_url, width, h);
						
						File fileNeg = new File(urls);
						if (fileNeg.exists()) {
							fileNeg.delete();
						}
					}
					
					_temp = "/" + new_url.replace(localUrl, "").replace("//", "/");
				}else {
					//图片的缩放后的宽高
					targetWidth = (int)((float)h/height*width);
					targetHeight = h;
					
					//生成目标图片大小
					Thumbnails.of(filesCheck).forceSize(targetWidth, targetHeight)
						.outputFormat(tail).outputQuality(1.0f).toFile(new_url);
					
					_temp = "/"+new_url.replace(localUrl, "").replace("//", "/");
				}
			}
		} catch (Exception e) {
			logger.info("切割图片失败"+url);
			_temp = DefaultImg(flag);
		}
		
		//判断需要的返回值类型
		if (value) {
			Map<String, Object> _map =  new HashMap<String, Object>();
			FileInputStream is;
			try {
				is = new FileInputStream(localUrl + _temp);
				BufferedImage sourceImg = javax.imageio.ImageIO.read(is);
				
				_map.put("url", _temp);
				_map.put("width", sourceImg.getWidth());
				_map.put("height", sourceImg.getHeight());
			} catch (Exception e) {
				logger.info("读取图片失败"+url);
			}
			
			return _map;
		}else {
			return _temp;
		}
	}
	
	
	/**
	 * 用途等比缩放图片
	 * @author 王国庆
	 * @param url  上传文件的目录地址
	 * @param local  缓存文件的除参数外的存放地址
	 * @param Q  缓存文件的图片地址0、低 1、中 2、高
	 * @param T 0、缩放不切割 1、缩放切割
	 * @param WxH 目的图片的大小
	 * @throws Exception
	 */
	private static Object geometricImg(String url, String WxH, int flag, boolean value, boolean filler) {
		String _temp = "";
		try {
			//判断url是否为空
			if (AllUtil.isNull(url) || AllUtil.isNull(WxH)) {
				url = DefaultImg(flag);
			}
			
			url = url.replaceAll("\\\\", "/");
			//获取文件名和文件类型
			String fileName = url.substring(url.lastIndexOf("/")+1, url.lastIndexOf("."));
			String ext = url.substring(url.lastIndexOf("."));
			
			String local = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/geometric/"+WxH+"/";	//切图的文件夹地址
			String new_url = local+fileName+ext;	//拼写切图的文件地址
			if (new_url.endsWith(".bmp")) {
				new_url = new_url.replace(".bmp", ".jpg");
			}
			
			File filesCheck = new File(localUrl + url);			//原图的文件流
			if (!filesCheck.exists() || filesCheck.length() == 0) {
				url = DefaultImg(flag);
				local = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/geometric/"+WxH+"/"+flag+"/";
				new_url = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/geometric/"+WxH+"/"+flag+"/default"+ext;
				if (new_url.endsWith(".bmp")) {
					new_url = new_url.replace(".bmp", ".jpg");
				}
				
				filesCheck = new File(localUrl + url);
			}
			
			File files = new File(new_url);						//切图的文件流
			
			/* 判断图片存在时是否切割 */
			if (files.exists() &&files.length() != 0) {
				_temp = "/"+new_url.replace(localUrl, "").replace("//", "/");
			} else if(ext.equals(".gif")){
				_temp = url;
			} else {
				if (!filesCheck.exists() || filesCheck.length() == 0) {
					filesCheck = new File(localUrl + DefaultImg(flag));
				}
				
				BufferedImage sourceImg = ImageIO.read(filesCheck);
				//图片的原始宽高
				int height = sourceImg.getHeight();
				int width = sourceImg.getWidth();
				//图片的目标宽高
				int w = Integer.parseInt(WxH.split("x")[0]);
				int h = Integer.parseInt(WxH.split("x")[1]);
				
				int targetWidth = 0;
				int targetHeight = 0;
				
				if (height < h && width <w) {
					targetWidth = width;
					targetHeight = height;
				}else {
					List<Integer> listWh = judge(w, h, height, width, 0);
					//图片的缩放后的宽高
					targetWidth = listWh.get(0);
					targetHeight = listWh.get(1);
				}
				
				File file = new File(local);
				if (!file.exists()) {
					file.mkdirs();
				}
				
				String tail = ext.substring(1);
				if (".bmp".equals(ext)) {
					tail = "jpg";
				}
				
				//生成目标图片大小
				Thumbnails.of(filesCheck).forceSize(targetWidth, targetHeight)
					.outputFormat(tail).outputQuality(1f).toFile(new_url);
				
				_temp = "/"+new_url.replace(localUrl, "").replace("//", "/");
			}
		} catch (Exception e) {
			logger.info("切割图片失败"+url);
			_temp = DefaultImg(flag);
		}
		
		//判断需要的返回值类型
		if (value) {
			Map<String, Object> _map =  new HashMap<String, Object>();
			FileInputStream is;
			try {
				is = new FileInputStream(localUrl + _temp);
				BufferedImage sourceImg = javax.imageio.ImageIO.read(is);
				
				_map.put("url", _temp);
				_map.put("width", sourceImg.getWidth());
				_map.put("height", sourceImg.getHeight());
			} catch (Exception e) {
				logger.info("读取图片失败"+url);
			}
			
			return _map;
		}else {
			return _temp;
		}
	}
	
	private static Object catchImg(String url, String WxH, int flag, boolean value, boolean filler, boolean cut){
		String _temp = "";
		try {
			//判断url是否为空
			if (AllUtil.isNull(url) || AllUtil.isNull(WxH)) {
				_temp = DefaultImg(flag);
			}
			
			url = url.replaceAll("\\\\", "/");
			//获取文件名和文件类型
			String fileName = url.substring(url.lastIndexOf("/")+1, url.lastIndexOf("."));
			String ext = url.substring(url.lastIndexOf("."));
			
			String local = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/WxH/"+WxH+"/";	//切图的文件夹地址
			String new_url = local+fileName+ext;	//拼写切图的文件地址
			if (new_url.endsWith(".bmp")) {
				new_url = new_url.replace(".bmp", ".jpg");
			}
			
			File filesCheck = new File(localUrl + url);			//原图的文件流
			if (!filesCheck.exists() || filesCheck.length() == 0) {
				url = DefaultImg(flag);
				local = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/WxH/"+WxH+"/"+flag+"/";
				new_url = localUrl+"cache"+url.substring(0,url.lastIndexOf("/"))+"/WxH/"+WxH+"/"+flag+"/default"+ext;
				if (new_url.endsWith(".bmp")) {
					new_url = new_url.replace(".bmp", ".jpg");
				}
				
				filesCheck = new File(localUrl + url);
			}
			
			File files = new File(new_url);						//切图的文件流
			
			/* 判断图片存在时是否切割 */
			if (files.exists() &&files.length() != 0) {
				_temp = "/"+new_url.replace(localUrl, "").replace("//", "/");
			} else if(ext.equals(".gif")){
				_temp = url;
			} else if (!filesCheck.exists() || filesCheck.length() == 0) {
				_temp = DefaultImg(flag);
			}else {
				BufferedImage sourceImg = ImageIO.read(filesCheck);
				//图片的原始宽高
				int height = sourceImg.getHeight();
				int width = sourceImg.getWidth();
				//图片的目标宽高
				int w = Integer.parseInt(WxH.split("x")[0]);
				int h = Integer.parseInt(WxH.split("x")[1]);
				
				int targetWidth = 0;
				int targetHeight = 0;
				
				File file = new File(local);
				if (!file.exists()) {
					file.mkdirs();
				}
				
				String tail = ext.substring(1);
				if (".bmp".equals(ext)) {
					tail = "jpg";
					ext = ".jpg";
				}
				
				if (!cut) {
					if (height < h && width <w) {
						targetWidth = w;
						targetHeight = h;
					}else {
						List<Integer> listWh = judge(w, h, height, width, 0);
						targetWidth = listWh.get(0);
						targetHeight = listWh.get(1);
					}
					
					Thumbnails.of(localUrl + url).forceSize(targetWidth, targetHeight)
						.outputFormat(tail).outputQuality(1f).toFile(new_url);
					
					if (filler) {
						String urls = negative(localUrl + url, w, h);
						new_url = mergePic(urls, new_url, w, h);
						
						File fileNeg = new File(urls);
						if (fileNeg.exists()) {
							fileNeg.delete();
						}
					}
					
					_temp = new_url;
				}else {
					List<Integer> listWh = judge(w, h, height, width, 1);
					targetWidth = listWh.get(0);
					targetHeight = listWh.get(1);
					
					Thumbnails.of(localUrl + url).forceSize(targetWidth, targetHeight)
						.outputFormat(tail).outputQuality(1f).toFile(new_url);
					
					//从图片中截取想要的图片
					int x = 0;
					int y = 0;
					if (targetWidth == w) {
						y = Math.abs((targetHeight-h))/2;
						_temp = cutPic(new_url, x, y, w, h);
					}
					if (targetHeight == h) {
						x = Math.abs((targetWidth-w))/2;
						_temp = cutPic(new_url, x, y, w, h);
					}
				}
				
				_temp = "/"+_temp.replace(localUrl, "").replace("\\", "/");
			}
		} catch (Exception e) {
			logger.info("切割图片失败"+url);
			_temp = DefaultImg(flag);
		}
		
		//判断需要的返回值类型
		if (value) {
			Map<String, Object> _map =  new HashMap<String, Object>();
			FileInputStream is;
			try {
				is = new FileInputStream(localUrl + _temp);
				BufferedImage sourceImg = javax.imageio.ImageIO.read(is);
				
				_map.put("url", _temp);
				_map.put("width", sourceImg.getWidth());
				_map.put("height", sourceImg.getHeight());
			} catch (Exception e) {
				logger.info("读取图片失败"+url);
			}
			
			return _map;
		}else {
			return _temp;
		}
	}
	
	
	/**
	 * 计算图片的改写宽高
	 * @author 王国庆
	 * @param w 目标宽度
	 * @param h 目标高度
	 * @param height 图片宽度
	 * @param width 图片高度
	 * @return
	 */
	private static List<Integer> judge(int w, int h, int height, int width, int T) {
		
		List<Integer> list = new ArrayList<Integer>();
		int targetWidth = 0;
		int targetHeight = 0;
		if (T == 0) {
			//如果是缩放不切割
			if ((double)w/width > (double)h/height) {
				targetHeight = h;
				targetWidth = (int)(((double)h/height)*width+0.5);
			}else {
				targetWidth = w;
				targetHeight = (int)(((double)w/width)*height+0.5);
			}
		}else {
			//如果是缩放切割
			if ((double)w/width > (double)h/height) {
				targetWidth = w;
				targetHeight = (int)(((double)w/width)*height+0.5);
			}else {
				targetHeight = h;
				targetWidth = (int)(((double)h/height)*width+0.5);
			}
		}
		
		list.add(targetWidth);
		list.add(targetHeight);
		
		return list;
	}
	
	
	/**
	 * 生成白色的打底图片
	 * @author 王国庆
	 * @return
	 */
	private static String negative(String url, int width, int height) throws Exception{
		String tempwUrl=url.substring(0,url.lastIndexOf("/"))+"/negative"+url.substring(url.lastIndexOf("."));
		
		File file = new File(tempwUrl);   
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);    
		Graphics2D g2 = (Graphics2D)bi.getGraphics();    
		g2.setBackground(Color.WHITE);    
		g2.clearRect(0, 0, width, height);  
		ImageIO.write(bi, url.substring(url.lastIndexOf(".")+1), file);
		
		return tempwUrl;
	}
	
	
	/** 
	 * 合并图片方法 
	 */
	private static String mergePic(String url1,String new_url, Integer w, Integer h) throws Exception{
		Thumbnails.of(url1).size(w, h)
	    	.watermark(Positions.CENTER, ImageIO.read(new File(new_url)), 1f)
	    	.outputQuality(1f).toFile(new_url);

		return new_url;
	}
	
	
	/**
	 * 切割图片
	 * @param url  原图片地址
	 * @param x   坐标
	 * @param y   坐标
	 * @param width    裁剪后图片宽度
	 * @param height   裁剪后图片宽度
	 * @return
	 * @throws IOException
	 */
	private static String cutPic(String url, int x, int y,int width,int height)throws Exception {
		Thumbnails.of(url).sourceRegion(x, y, width, height)
        	.size(width, height).keepAspectRatio(false).toFile(url);
		
		return  url;
	}
	
	public static String GetCropImg(String url, int xnum, int ynum, int wnum, int hnum, int width, int height){
		return GetCutPic(url, xnum, ynum, wnum, hnum, width, height, false);
	}
	
	public static String GetCropImg(String url, int xnum, int ynum, int wnum, int hnum){
		return GetCutPic(url, xnum, ynum, wnum, hnum, 0, 0, true);
	}
	
	/**
	 * 计算比率从原图中扣取图片
	 * @param url	图片地址
	 * @param xnum	左上x坐标
	 * @param ynum	左上y坐标
	 * @param wnum	截取框宽度
	 * @param hnum	截取框高度
	 * @param width	图片压缩的宽度
	 * @param height 图片压缩的高度
	 * @param calculation	坐标是否已计算
	 * @author WangGQ
	 */
	private static String GetCutPic(String url, int xnum, int ynum, int wnum, int hnum, int width, int height, boolean calculation){
		String _temp = "";
		
		try {
			if (AllUtil.isNull(url)) {
				_temp = DefaultImg(1);
			}
			
			String new_url = localUrl + url;
			File file = new File(localUrl + url);
			
			if (null== file || file.length() == 0 || file.length() >= 20000000) {
				_temp = DefaultImg(1);
			}else {
				int x = xnum;
				int y = ynum;
				int w = wnum;
				int h = hnum;
				if (!calculation) {
					BufferedImage bufferedImage = ImageIO.read(file);
					int width_origin = bufferedImage.getWidth();
					int height_origin = bufferedImage.getHeight();
					
					float percent = 1;
					if (width_origin < width && height_origin < height) {
						percent = 1;
					}else if(width_origin < height_origin){
						percent = Float.valueOf(height_origin)/height;
					}else if(width_origin >= height_origin){
						percent = Float.valueOf(width_origin)/width;
					}
					
					x = (int)(Float.valueOf(xnum)*percent);
					y = (int)(Float.valueOf(ynum)*percent);
					w = (int)(Float.valueOf(wnum)*percent);
					if (w > width_origin) {
						w = width;
					}
					h = (int)(Float.valueOf(hnum)*percent);
					if (h > height_origin) {
						h = height;
					}
				}
				_temp = cutPic(new_url, x, y, w, h).replace(localUrl, "").replace("\\", "/");
			}
			
		} catch (Exception e) {
			logger.info("读取图片失败"+url);
			_temp = DefaultImg(1);
		}
		
		return _temp;
	}
	
	/**
	 * Thumbnails 高质量等比缩放裁切图片,坚决不补白
	 * @author 	韩晗<br/>
	 * @param 	originUrl	源图片地址<br/>
	 * @param 	WxH			目标图片宽高<br/>
	 * @param 	isLessThan	缩放图片是否保持在WxH范围内<br/>
	 * 						true	适合于原图等比缩小，目标图片等比缩放，尺寸一定小于等于WxH，但不补白<br/>
	 * 						false	适合头像、Logo等缩略图，目标图片按较大比例缩放，溢出部分被裁切<br/>
	 * @param 	isWH		目标图片名称结尾是否拼接图片且后宽高，格式 '*_WxH.jpg'<br/>
	 * @param 	errThan		出现错误后的处理方式（因采用线程方式，已没有太大作用）<br/>
	 * 						true	返回源图<br/>
	 * 						false	返回一张可爱的替代图<br/>
	 * @return	targetUrl	目标图片路径
	 * @return 				gif直接返回不裁切
	 * 						其他按设定缩放裁切并转为jpg图片
	 * @throws 	Exception
	 */
	public static String cropImg(String originUrl, String WxH, boolean isLessThan, boolean isWH, boolean errThan) {
		
		// 传参错误，返回null
		if(null==originUrl || "".equals(originUrl.trim())) {
			return "/upload/cry.jpg";
		}
		
		// 网站根目录物理地址
		String rootPath = localUrl;
		
		// 源文件不存在或长度为0，返回null
		File originFile = new File(rootPath + originUrl);
		if (!originFile.exists() || originFile.length()==0) {
			return "/upload/cry.jpg";
		}
		
		try{
			// 取源图片后缀
			String ext = originUrl.substring(originUrl.lastIndexOf(".")+1).toLowerCase();
			if("gif".equals(ext)) {
				return originUrl;
			}
			
			// 开始拼接目标图片物理地址
			String filename = originUrl.substring(originUrl.lastIndexOf("/")+1, originUrl.lastIndexOf("."));			// 图片名，以jpg结尾的
			if(isWH) {
				filename += "_"+WxH+".jpg";
			}
			else {
				filename += ".jpg";
			}
			
			String targetPath = "";	// 目标图片路径的目录部分
			if(isLessThan) {
				targetPath = rootPath + "cache" + originUrl.substring(0, originUrl.lastIndexOf("/"))+"/"+WxH+"/95/0/";
			}
			else {
				targetPath = rootPath + "cache" + originUrl.substring(0, originUrl.lastIndexOf("/"))+"/"+WxH+"/95/1/";
			}
			File targetFile = new File(targetPath + filename);
			
			// 创建目录
			File targetDir = new File(targetPath);
			if (!targetDir.exists()) {
				targetDir.mkdirs();
			}
		
			// 原始图片需要先缩放再切割
			final String 	final_WxH = WxH;
			final File 	 	final_targetFile = targetFile;
			final File 	 	final_originFile = originFile;
			final boolean	final_isLessThan = isLessThan;
			
			// 开启新线程去处理切图
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			executorService.submit(new Runnable() {
			    public void run() {
			    	try {
			    		// 如果图片已存在，则直接返回
			    		if (final_targetFile.exists() && final_targetFile.length()!=0) {
			    			return;
			    		}
			    		
			    		// 取宽高
						int W  = Integer.parseInt(final_WxH.split("x")[0]);
						int H = Integer.parseInt(final_WxH.split("x")[1]);
			    		
			    		// 如果是保持范围内裁切
			    		if(final_isLessThan) {
			    			// 生成目标图片大小
							Thumbnails.of(final_originFile.getPath())
									  .size(W, H)
									  .outputFormat("jpg")
									  .outputQuality(0.95f) 				// 生成质量
									  .toFile(final_targetFile.getPath());
			    		}
			    		// 保持图片比例，溢出裁切
			    		else {
			    			// 读图
							FileInputStream is = new FileInputStream(final_originFile.getPath());
							BufferedImage bi = javax.imageio.ImageIO.read(is);
							
							// 如果原图小于目标宽高，则不裁切了，直接返回
							if(W>bi.getWidth() && H>bi.getHeight()) {
								return;
							}
							
							// 切起来
						    if(bi.getWidth()/bi.getHeight() > W/H) {
								bi = Thumbnails.of(final_originFile.getPath()).height(H).asBufferedImage();
							}
							else {
								bi = Thumbnails.of(final_originFile.getPath()).width(W).asBufferedImage();
							}
						    Thumbnails.of(bi)
						    		  .sourceRegion(Positions.CENTER, W, H)
						    		  .size(W, H)
						    		  .outputFormat("jpg")
						    		  .outputQuality(0.95f) 				// 生成质量
									  .toFile(final_targetFile.getPath());
			    		}
			    	} 
			    	catch (Exception e) {
						logger.error("请及时处理图片切割出错，原始图片（"+final_originFile.getPath()+"），目标图片（"+final_targetFile.getPath()+"）", e);
					}
			    }
			});
			
			return "/"+targetFile.getPath().substring(targetFile.getPath().indexOf("cache")).replace("\\", "/");
		} 
		catch (Exception e) {
			logger.error("请及时处理图片切割出错，原始图片（"+originFile.getPath()+"），切图参数：("+WxH+","+isLessThan+")");
			if(errThan) {
				return originUrl;
			}
			else {
				return "/upload/cry.jpg";
			}
		}
	}
	
	/**
	 * 切用户头像(等比)
	 * @param originPath 原始图片路径
	 * @param size 宽高 "72x72"
	 * @return
	 * @author 王涛
	 * 2015年8月13日
	 */
	public static String thumbnailLogo(String originPath, String size) {
		return (String) geometricImg(originPath, size, Picture.FLAG_LOGO, Picture.VALUE_URL, Picture.IS_NO_FILLER);
	}
	
	/**
	 * 切商品(等比)
	 * @param originPath 原始图片路径
	 * @param size 宽高 "72x72"
	 * @return
	 * @author 王涛
	 * 2015年8月13日
	 */
	public static String thumbnailMall(String originPath, String size) {
		return (String) geometricImg(originPath, size, Picture.FLAG_YISINET, Picture.VALUE_URL, Picture.IS_NO_FILLER);
	}
	
	/**
	 * 切圈子logo(等比)
	 * @param originPath 原始图片路径
	 * @param size 宽高 "72x72"
	 * @return
	 * @author 王涛
	 * 2015年8月13日
	 */
	public static String thumbnailOg(String originPath, String size) {
		return (String) geometricImg(originPath, size, Picture.FLAG_OG, Picture.VALUE_URL, Picture.IS_NO_FILLER);
	}
}
