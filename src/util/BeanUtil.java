package util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 对象转换工具类
 * @author 王子莅
 * @edit 王涛
 */

public class BeanUtil {

	/**
	 * 将任意对象转为Map（全部转换）
	 * 字段不转义
	 * @param obj 待转换换对象
	 * @return 对象转换后的map
	 */
	public static Map<String, Object> convertObjectToMap(Object obj) {
		return convertObjectToMap(obj, false, null);
	}

	/**
	 * 将任意对象转为Map（全部转换）
	 * @param obj 待转换对象
	 * @param isEscape 是否需要转义
	 * @return 对象转换后的map
	 */
	public static Map<String, Object> convertObjectToMap(Object obj, Boolean isEscape) {
		return convertObjectToMap(obj, isEscape, null);
	}
	
	/**
	 * 将任意对象转为Map（全部转换）
	 * @param obj 待转换对象
	 * @param isEscape 是否需要转义
	 * @param clazz 对象的class
	 * @return 对象转换后的map
	 */
	private static Map<String, Object> convertObjectToMap(Object obj, Boolean isEscape, Class<?> clazz) {
		Map<String, Object> result = new HashMap<String, Object>();

		// 取得对象的class
		Class<?> oClass = clazz != null ? clazz : obj.getClass();

		// 取得所有的字段进行遍历
		for (Field field : oClass.getDeclaredFields()) {
			try {

				// 如果声明为private，需要修改访问权限
				field.setAccessible(true);

				// 取得字段值，按字段名和值存入map
				if (field.get(obj) != null) {
					// 判断该属性是否是基本类型, 若非基本类型, 递归调用转map方法
					if (AllUtil.isPrimitive(field)) {
						if(isEscape) {
							result.put(field.getName(), StrUtil.escapeHTML(field.get(obj).toString()));
						} else {
							result.put(field.getName(), field.get(obj).toString());
						}
					} else {
						result.put(field.getName(), convertObjectToMap(field.get(obj), isEscape));
					}
				} else {
					result.put(field.getName(), null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Class<?> superClazz = oClass.getSuperclass();
		if (superClazz != null) {
			// 取得所有的字段进行遍历
			for (Field field : superClazz.getDeclaredFields()) {

				try {

					// 如果声明为private，需要修改访问权限
					field.setAccessible(true);

					// 取得字段值，按字段名和值存入map
					if (field.get(obj) != null) {
						if(isEscape) {
							result.put(field.getName(), StrUtil.escapeHTML(field.get(obj).toString()));
						} else {
							result.put(field.getName(), field.get(obj).toString());
						}
						
					} else {
						result.put(field.getName(), null);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	/**
	 * 将任意List&lt;?&gt; 转为List&lt;Map&gt;（全部属性转换）
	 * @param List 待转换的list
	 * @return 对象转换后的list
	 */
	public static List<Map<String, Object>> convertObjectToMapList(List<?> list) {
		return convertObjectToMapList(list, false);
	}
	
	/**
	 * 将任意List Object 转为List Map（全部属性转换）
	 * @param List
	 * @param isEscape 是否转义属性
	 * @return
	 */
	public static List<Map<String, Object>> convertObjectToMapList(List<?> list, Boolean isEscape) {
		
		List<Map<String, Object>> result = new ArrayList<>();
		
		if (list.size() == 0) return result;
		
		// 取得对象的class
		Class<?> clazz = list.get(0).getClass();
		
		for (Object o : list) {
			result.add(convertObjectToMap(o, isEscape, clazz));
		}
		return result;
	}
	

	/**
	 * 将任意对象转为Map（按需转换）
	 * @param obj
	 * @param requires 需要存入得字段名集合，必须跟对象的字段名一致
	 * @return
	 */
	public static Map<String, Object> convertRequiredObjectToMap(Object obj, Set<String> requires) {

		Map<String, Object> result = new HashMap<String, Object>();

		// 取得对象的class
		Class<?> oClass = obj.getClass();

		// 取得所有的字段进行遍历
		for (Field field : oClass.getDeclaredFields()) {

			try {

				// 如果声明为private，需要修改访问权限
				field.setAccessible(true);

				// 如果当前字段是需要存入， 取得字段值，按字段名和值存入map
				if (requires.contains(field.getName()) && field.get(obj) != null) {
					result.put(field.getName(), field.get(obj));
				} else {
					result.put(field.getName(), null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * 将任意对象转为Map（排除转换）
	 * @param obj
	 * @param exceptions 不需要存入的字段集合，必须跟对象的字段名一致
	 * @return
	 */
	public static Map<String, Object> convertObjectToMapExceptArgs(Object obj, Set<String> exceptions) {
		Map<String, Object> result = new HashMap<String, Object>();

		// 取得对象的class
		Class<?> oClass = obj.getClass();

		// 取得所有的字段进行遍历
		for (Field field : oClass.getDeclaredFields()) {

			try {

				// 如果声明为private，需要修改访问权限
				field.setAccessible(true);

				// 如果当前字段是需要存入， 取得字段值，按字段名和值存入map
				if (!exceptions.contains(field.getName()) && field.get(obj) != null) {
					result.put(field.getName(), field.get(obj));
				} else {
					result.put(field.getName(), null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return result;
	}
	
//	public static void copyProperties(List<?> sourceList, List<?> targetList) throws InstantiationException, IllegalAccessException {
//		for (Object source : sourceList) {
//			// targetList.getClass().
//			//Object target = BeanUtils.copyProperties(source, target);
//		}
//		
//		
//	}

}
